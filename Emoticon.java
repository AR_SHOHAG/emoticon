import java.awt.*;
import java.applet.*;

public class Emoticon extends Applet
{
	public void paint(Graphics g)
	{
		Color C1 = new Color(255,255,51);
		Color C2 = new Color(255,153,153);

		g.setColor(Color.black);
		g.drawOval(100.5,100.8,70.5,70.5);
		g.setColor(C1);
		g.fillOval(100,100,70,70);
		g.setColor(Color.black);
		g.fillOval(118,118,10,14);
		g.fillOval(144,118,10,14);
		g.drawLine(124,145,148,145);
		g.drawLine(124,146,148,146);
		g.setColor(C2);
		g.fillArc(136,134,12,26,180,180);

	}
}